# Graph assembler

# Установка

* Установить Anaconda3
* Создать окружение для работы с помощью команды **conda env create -f environment.yml**
* Установка движка способ 1
  * Скачать [архив](https://anaconda.org/projectchrono/pychrono/7.0.0/download/win-64/pychrono-7.0.0-py39_2112.tar.bz2) и поместить в папку с репозиторием
  * Активировать окружение lic_ai **conda activate lic_ai**
  * Установить Pychono из архива **conda install pychrono-7.0.0-py39_2112.tar.bz2**

* Установка движка способ 2
  * conda install -c projectchrono pychrono
  
# Прокидываем дисплей через Docker

* Установить Х сервер для Windows https://sourceforge.net/projects/vcxsrv/
* В образе уже указаны необходимые вещи для работы с дисплеем
